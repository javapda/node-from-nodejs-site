/**
 * https://nodejs.org/en/docs/guides/getting-started-guide/
 */
var opn = require('opn');

 const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});

server.listen(port, hostname, () => {
  const url=`http://${hostname}:${port}/`;
  console.log(`Server running at ${url}`);
  // opens the url in the default browser 
  opn(url);
  // opn(url, {app: 'firefox'});
  // opn(url, {app: 'chrome'});
  //opn(url, {app: 'safari'});


});