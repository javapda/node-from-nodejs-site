const timeoutObj = setTimeout(() => {
    console.log('timeout beyond time');
  }, 1500);
  
  const immediateObj = setImmediate(() => {
    console.log('immediately executing immediate');
  });
  
  const intervalObj = setInterval(() => {
    console.log('interviewing the interval');
  }, 500);
  
  function clearThem() {
    clearTimeout(timeoutObj);
    clearImmediate(immediateObj);
    clearInterval(intervalObj);
  }

  // wait a few seconds and clear them all
  setTimeout(clearThem, 4000)