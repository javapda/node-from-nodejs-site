/*
run using:
node --experimental-modules src/experimental/es6-write-to-file.mjs

url: 
 https://nodejs.org/api/esm.html
note:
 file must end with '.mjs'
*/
import fs from 'fs'
console.log('testing '+fs)
const data = "\n\nthis is data"
fs.writeFile('/tmp/message.txt', data, {flag:'a+'}, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });