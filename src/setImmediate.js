/**
 * setImmediate runs after current event loop
 */
console.log('before immediate');

const immediate = setImmediate((arg) => {
  console.log(`executing immediate: ${arg}`);
}, 'so immediate');
setTimeout(()=>{
    console.log(`'immediate' returned from setImmediate: ${immediate}`)
},1000);

console.log('after immediate');