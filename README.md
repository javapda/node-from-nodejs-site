# node-from-nodejs-site #

* sample node snippets from the nodejs site

# setup #

* install nodejs
* pull this git project
* run:  npm install
* run programs e.g.: **_node src/hello-world-http-server.js_**

# programs #

* hello-world-http-server.js : simple http server serving up hello world on port 3000
* [setTimeout.js](https://github.com/nodejs/nodejs.org/blob/master/locale/en/docs/guides/timers-in-node.md) : demo setTimeout
* [setImmediate.js](https://github.com/nodejs/nodejs.org/blob/master/locale/en/docs/guides/timers-in-node.md) : demo setImmediate
* [clearTimerImmediateAndInterval.js](https://github.com/nodejs/nodejs.org/blob/master/locale/en/docs/guides/timers-in-node.md) : demo clearing timer, immediate, interval